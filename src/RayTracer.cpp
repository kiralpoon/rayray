// The main ray tracer.

#pragma warning (disable: 4786)

#include "RayTracer.h"
#include "scene/light.h"
#include "scene/material.h"
#include "scene/ray.h"

#include "parser/Tokenizer.h"
#include "parser/Parser.h"

#include "ui/TraceUI.h"
#include <cmath>
#include <algorithm>

// Added for Brian
#include <stdio.h>
#include <string.h>

extern TraceUI* traceUI;

#include <iostream>
#include <fstream>
#include "./ui/ModelerCamera.h"
#include <math.h>

using namespace std;

// Use this variable to decide if you want to print out
// debugging messages.  Gets set in the "trace single ray" mode
// in TraceGLWindow, for example.
bool debugMode = false;

// Trace a top-level ray through normalized window coordinates (x,y)
// through the projection plane, and out into the scene.  All we do is
// enter the main ray-tracing method, getting things started by plugging
// in an initial ray weight of (0.0,0.0,0.0) and an initial recursion depth of 0.
Vec3d RayTracer::trace( double x, double y )
{
	// Clear out the ray cache in the scene for debugging purposes,
	scene->intersectCache.clear();

    ray r( Vec3d(0,0,0), Vec3d(0,0,0), ray::VISIBILITY );

    scene->getCamera().rayThrough( x,y,r );
	Vec3d ret = traceRay( r, Vec3d(1.0,1.0,1.0), 0 );
	ret.clamp();
	return ret;
}

// Do recursive ray tracing!  You'll want to insert a lot of code here
// (or places called from here) to handle reflection, refraction, etc etc.
Vec3d RayTracer::traceRay( const ray& r, 
	const Vec3d& thresh, int depth )
{
	isect i;
	Vec3d vTotal;
	int maxDepth = traceUI->getDepth();

	if( scene->intersect( r, i ) ) {
		// YOUR CODE HERE

		// An intersection occured!  We've got work to do.  For now,
		// this code gets the material for the surface that was intersected,
		// and asks that material to provide a color for the ray.  

		// This is a great place to insert code for recursive ray tracing.
		// Instead of just returning the result of shade(), add some
		// more steps: add in the contributions from reflected and refracted
		// rays.

		const Material& m = i.getMaterial();
	 
		vTotal = m.shade(scene, r, i);

		if (depth >= maxDepth) {
			/*Stop recursion*/
			return vTotal;
		}
		else {
			Vec3d vIncident = -r.getDirection();

			/* Reflecion handling*/
			if (!m.kr(i).iszero()) {
				Vec3d reflectedDir = 2*(i.N*vIncident)*i.N-vIncident;
				reflectedDir.normalize();
				ray reflectedRay(r.at(i.t), reflectedDir, ray::REFLECTION);
				Vec3d tmp = traceRay(reflectedRay, thresh, depth+1);
				tmp[0] = tmp[0]*m.kr(i)[0];
				tmp[1] = tmp[1]*m.kr(i)[1];
				tmp[2] = tmp[2]*m.kr(i)[2];
				vTotal += tmp;
			}
			
			/* Refraction handling*/
			double n;
			double f;
			if (r.getDirection()*i.N<0) {
				/* Air to Object*/
				n = 1.0/m.index(i);
				f=1;
			}
			else {
				/* Object to Air*/
				n = m.index(i);
				f =-1;
			}
			double c1  = vIncident*(i.N*f);
			double c2 = 1.0 - (n*n)*(1.0-c1*c1);
			Vec3d tr;
	
			if ((c2 > 0.0)&&(!m.kt(i).iszero()))
			{
				c2 = sqrt(c2);
				tr = (n*c1-c2)*(i.N*f)-n*vIncident;
				tr.normalize();
				
				//ray refractedRay(r.at(i.t)+tr*0.0001, tr, ray::REFRACTION);
				ray refractedRay(r.at(i.t), tr, ray::REFRACTION);
				Vec3d transCol = traceRay(refractedRay, thresh, depth+1);
				transCol[0] = transCol[0]*m.kt(i)[0];
				transCol[1] = transCol[1]*m.kt(i)[1];
				transCol[2] = transCol[2]*m.kt(i)[2];
				vTotal += transCol;
			}
			#if 0
			else if ((c2<0.0)&&(!m.kt(i).iszero()))
			{
				/*Total internal reflection*/
				Vec3d inReflectedDir = 2*(i.N*vIncident)*(i.N)-vIncident;
				inReflectedDir.normalize();
				ray inRefelctedRay(r.at(i.t), inReflectedDir, ray::REFLECTION);
				Vec3d tmp1 = traceRay(inRefelctedRay, thresh, depth+1);
				if (!tmp1.iszero()) {
					vTotal += tmp1;
				}
			}
			#endif
			
			return vTotal;
		}
	} else {
		// No intersection.  This ray travels to infinity, so we should color
		// it black if no background image is loaded, otherwise we should color
		// it according to the background image.

		if (traceUI->getUseBackground()) { // A background image is loaded

			// We will map the background image to a cubic mapping.
			// For cubic mapping, the convention will be the following: (from Watt pg. 246)
			/*
			 * 				---------
			 * 				|		|
			 * 				|top	|
			 * 				|+z,+x	|
			 *     ----------------------------------
			 *     |		|		|		|		|
			 *     |left	|front	|right	|back	|
			 *     |+y,-z	|+y,+x	|+y,+z	|+y,-x	|
			 *     ----------------------------------
			 *     			|		|
			 *     			|bottom	|
			 *     			|-z,+x	|
			 *     			---------
			 */

			Vec3d backColor = Vec3d( 1.0, 0.0, 0.0 );  // default to red for debugging
			Vec3d rayDirection = r.getDirection();  // 3d vector of ray direction
			rayDirection.normalize();
				// rayDirection[0] = x
				// rayDirection[1] = y
				// rayDirection[2] = z
			float u,v = 0;	// parametric coordinates on texture face

			// Determine the face of the cubic mapping needed based on the magnitude
			// of the ray direction that is traveling towards infinity.
			// If the ray direction has greatest magnitude along the x-axis
			if (fabs(rayDirection[0]) >= fabs(rayDirection[1]) && fabs(rayDirection[0]) >= fabs(rayDirection[2])) {
				if (rayDirection[0] > 0.0f){  // traveling on +x axis (right)
					u = 1.0f - (rayDirection[2] / rayDirection[0] + 1.0f) * 0.5f;  // eq. +z axis
					v = (rayDirection[1] / rayDirection[0] + 1.0f) * 0.5f;  // eq. +y axis
					// Read the corresponding texture from the cubic mapping of the background image
					backColor = readBackground(0, traceUI->getBackBuf(), u, v, traceUI->getBackWidth(), traceUI->getBackHeight());
				}
				else if (rayDirection[0] < 0.0f){ // traveling on -x axis (left)
					u = 1.0f - (rayDirection[2] / rayDirection[0] + 1.0f) * 0.5f;
					v = 1.0f - (rayDirection[1] / rayDirection[0] + 1.0f) * 0.5f;
					backColor = readBackground(1, traceUI->getBackBuf(), u, v, traceUI->getBackWidth(), traceUI->getBackHeight());
				}
			} else if ((fabs(rayDirection[1]) >= fabs(rayDirection[0])) && (fabs(rayDirection[1]) >= fabs(rayDirection[2]))){  // The ray direction has greatest magnitude along the y-axis
				if (rayDirection[1] > 0.0f) {  // traveling on the +y axis (top)
					u = (rayDirection[0] / rayDirection[1] + 1.0f) * 0.5f;
					v = 1.0f - (rayDirection[2]/ rayDirection[1] + 1.0f) * 0.5f;
					backColor = readBackground(2, traceUI->getBackBuf(), u, v, traceUI->getBackWidth(), traceUI->getBackHeight());
				}
				else if (rayDirection[1] < 0.0f) { // traveling on the -y axis (bottom)
					u = 1.0f - (rayDirection[0] / rayDirection[1] + 1.0f) * 0.5f;
					v = (rayDirection[2]/rayDirection[1] + 1.0f) * 0.5f;
					backColor = readBackground(3, traceUI->getBackBuf(), u, v, traceUI->getBackWidth(), traceUI->getBackHeight());
				}
			} else if ((fabs(rayDirection[2]) >= fabs(rayDirection[0])) && (fabs(rayDirection[2]) >= fabs(rayDirection[1]))){ // The ray direction has greatest magnitude along the z-axis
				if (rayDirection[2] > 0.0f) {  // traveling on the +z axis (front)
					u = (rayDirection[0] / rayDirection[2] + 1.0f) * 0.5f;
					v = (rayDirection[1]/rayDirection[2] + 1.0f) * 0.5f;
					backColor = readBackground(4, traceUI->getBackBuf(), u, v, traceUI->getBackWidth(), traceUI->getBackHeight());
				}
				else if (rayDirection[2] < 0.0f) { // traveling on the -z axis (back)
					u = (rayDirection[0] / rayDirection[2] + 1.0f) * 0.5f;
					v = 1.0f - (rayDirection[1] /rayDirection[2]+1) * 0.5f;
					backColor = readBackground(5, traceUI->getBackBuf(), u, v, traceUI->getBackWidth(), traceUI->getBackHeight());
				}
			} else {
				// Do something about equal magnitudes?
			}

			//cout << "backColorR: " << backColor[0] << "  G: " << backColor[1] << "  B: " << backColor[2] << endl;
			return backColor;

		} else {  // A background image is not loaded
			return Vec3d( 0.0, 0.0, 0.0 );
		}
	}
}

Vec3d RayTracer::readBackground(int face, unsigned char* buf, float u, float v, int picWidth, int picHeight){
		u = fabs(u);
	    v = fabs(v);
	    float faceSizeX = (float)picWidth / 4.0;  // Based on the 'cube' format
	    float faceSizeY = (float)picHeight / 3.0;
	    float leftBound, botBound;  // Define the bounding box for the correct face in the cube map


	    switch (face) {
	    case 0:
	    	leftBound = faceSizeX * 2;  // Move 2 faces over in the cube map
	    	botBound = faceSizeY;  // Move 1 face up in the cube map
	    	break;
	    case 1:
	    	leftBound = 0.0;
	      	botBound = faceSizeY;
	      	break;
	    case 2:
	    	leftBound = faceSizeX;
	    	botBound = faceSizeY * 2;
	    	break;
	    case 3:
	    	leftBound = faceSizeX;
	    	botBound = 0.0;
	    	break;
	    case 4:
	    	leftBound = faceSizeX;
	    	botBound = faceSizeY;
	    	break;
	    case 5:
	    	leftBound = faceSizeX * 3;
	    	botBound = faceSizeY;
	    	break;
	    default:
	    	break;
	    }

	    int backPixelX = (int) (leftBound + u * faceSizeX);
	    int backPixelY = (int) (botBound + v * faceSizeY);
	    float outputR = buf[(backPixelY * picWidth + backPixelX) * 3 ] / 255.;
	    float outputG = buf[(backPixelY * picWidth + backPixelX) * 3 + 1] / 255.;
	    float outputB = buf[(backPixelY * picWidth + backPixelX) * 3 + 2] / 255.;

	    Vec3d output = Vec3d(outputR, outputG, outputB);  // All values between 0 and 1

	    // Perform Gamma correction
	    // Used a hack fix to stop over/underflows
	    float gamma = traceUI->getBackGamma();
	    if (gamma >= 1) {
	       output[0] = (float) powf(output[0], gamma);
	       output[1] = (float) powf(output[1], gamma);
	       output[2] = (float) powf(output[2], gamma);
	    } else {
	    	output[0] = (float) powf(output[0], 1./((1.-gamma) * 5. * ((1.-gamma)*10.)));
	        output[1] = (float) powf(output[1], 1./((1.-gamma) * 5. * ((1.-gamma)*10.)));
	        output[2] = (float) powf(output[2], 1./((1.-gamma) * 5. * ((1.-gamma)*10.)));
	    }

	    return output;
}

RayTracer::RayTracer()
	: scene( 0 ), buffer( 0 ), buffer_width( 256 ), buffer_height( 256 ), m_bBufferReady( false )
{

}


RayTracer::~RayTracer()
{
	delete scene;
	delete [] buffer;
}

void RayTracer::getBuffer( unsigned char *&buf, int &w, int &h )
{
	buf = buffer;
	w = buffer_width;
	h = buffer_height;
}

double RayTracer::aspectRatio()
{
	return sceneLoaded() ? scene->getCamera().getAspectRatio() : 1;
}

bool RayTracer::loadScene( char* fn )
{
	ifstream ifs( fn );
	if( !ifs ) {
		string msg( "Error: couldn't read scene file " );
		msg.append( fn );
		traceUI->alert( msg );
		return false;
	}
	
	// Strip off filename, leaving only the path:
	string path( fn );
	if( path.find_last_of( "\\/" ) == string::npos )
		path = ".";
	else
		path = path.substr(0, path.find_last_of( "\\/" ));

	// Call this with 'true' for debug output from the tokenizer
	Tokenizer tokenizer( ifs, false );
    Parser parser( tokenizer, path );
	try {
		delete scene;
		scene = 0;
		scene = parser.parseScene();
	} 
	catch( SyntaxErrorException& pe ) {
		traceUI->alert( pe.formattedMessage() );
		return false;
	}
	catch( ParserException& pe ) {
		string msg( "Parser: fatal exception " );
		msg.append( pe.message() );
		traceUI->alert( msg );
		return false;
	}
	catch( TextureMapException e ) {
		string msg( "Texture mapping exception: " );
		msg.append( e.message() );
		traceUI->alert( msg );
		return false;
	}

	scene->computeBoundingBox();  // Computes a min/max bounding box for the whole scene
	scene->setRayTracer(this);  // Allows the scene access to the ray tracer and UI.

	if( ! sceneLoaded() )
		return false;

	return true;
}

void RayTracer::traceSetup( int w, int h )
{
	if( buffer_width != w || buffer_height != h )
	{
		buffer_width = w;
		buffer_height = h;

		bufferSize = buffer_width * buffer_height * 3;
		delete [] buffer;
		buffer = new unsigned char[ bufferSize ];


	}
	memset( buffer, 0, w*h*3 );
	m_bBufferReady = true;
}

void RayTracer::tracePixel( int i, int j )
{
	Vec3d col;
	int nSamples=traceUI->getAASampleRate();
	double inc = 0; 

	if (nSamples >1)
		inc= 0.4/(double)(nSamples-1);

	if( ! sceneLoaded() )
		return;

	double x_n = double(i-0.2)/double(buffer_width);
	double y_n = double(j-0.2)/double(buffer_height);

	if (x_n<0)
		x_n=0;

	if (y_n<0)
		y_n=0;

	if (nSamples ==1) {
		col += trace((double)i/(double)buffer_width,(double)j/(double)buffer_height);

	}
	else {
		for (int icnt=0;icnt<nSamples;icnt++) {
		for (int jcnt=0;jcnt<nSamples;jcnt++) {
			col += trace(x_n + inc*(double)icnt/(double)buffer_width,y_n +inc*(double)jcnt/(double)buffer_height);
		}
		}
		col /= (double)(nSamples*nSamples);
	}

	unsigned char *pixel = buffer + ( i + j * buffer_width ) * 3;

	pixel[0] = (int)( 255.0 * col[0]);
	pixel[1] = (int)( 255.0 * col[1]);
	pixel[2] = (int)( 255.0 * col[2]);
}


