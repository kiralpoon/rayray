#ifndef __RAYTRACER_H__
#define __RAYTRACER_H__

// The main ray tracer.

#include "scene/ray.h"
#include "ui/TraceUI.h"  // ADD

class Scene;

class RayTracer
{
public:
    RayTracer();
    ~RayTracer();

    Vec3d trace( double x, double y );
	Vec3d traceRay( const ray& r, const Vec3d& thresh, int depth );


	void getBuffer( unsigned char *&buf, int &w, int &h );
	double aspectRatio();
	void traceSetup( int w, int h );
	void tracePixel( int i, int j );

	bool loadScene( char* fn );

	bool sceneLoaded() { return scene != 0; }

    void setReady( bool ready )
      { m_bBufferReady = ready; }
    bool isReady() const
      { return m_bBufferReady; }

	const Scene& getScene() { return *scene; }

	void setUI( TraceUI* ui ) { UI = ui; } // ADD
	TraceUI* UI; // ADD

private:
	Vec3d readBackground(int face, unsigned char* buf, float u, float v, int picWidth, int picHeight);
	unsigned char *buffer;
	int buffer_width, buffer_height;
	int bufferSize;
	Scene* scene;

    bool m_bBufferReady;

    enum {
    					TOP = 0,
    					BOTTOM = 1,
    					RIGHT = 2,
    					LEFT = 3,
    					FRONT = 4,
    					BACK = 5
    				};


};

#endif // __RAYTRACER_H__
