// This code is partially based on suggestions from
// Mike Gerbush, UT Ph.D. student.
#include <cmath>

#include "kdTree.h"

using namespace std;

bool kdTree::intersect( const ray& r, isect& i, int& c )
{
  double tMin, tMax;
  double tLeftMin = 9999.0, tRightMin = 9999.0;
  double tLeftMax = -9999.0, tRightMax = -9999.0;

  if(this==NULL) {
    return false;
  }

  if(!bounds->intersect(r, tMin, tMax)) {
    return false;
  }

  if(isLeaf()) {
    bool hasIntersection = false;
    isect cur;
    typedef set<Geometry*>::const_iterator iter;
    for( iter j = objects.begin(); j != objects.end(); ++j ) {
      if( (*j)->intersect( r, cur ) ) {
        if( !hasIntersection || (cur.t < i.t) ) {
          i = cur;
          hasIntersection = true;
        }
      }
      c++;
    }
    if(!hasIntersection) { i.setT(9999.0); }
    return hasIntersection;
  }

    isect left, right;
    left.setT(9999.0);
    right.setT(9999.0);
    leftChild->intersect(r, left, c);
    rightChild->intersect(r, right, c);

    if((left.t == 9999.0) && (right.t == 9999.0)) {
      return false;
    }
    else if(right.t == 9999.0) {
      i = left;
      return true;
    }
    else if(left.t == 9999.0) {
      i = right;
      return true;
    }
    else if(left.t == right.t) {
      i = right;
      return true;
    }
    else if(left.t < right.t) {
      i = left;
      return true;
    }
    else if(left.t > right.t) {
      i = right;
      return true;
    }
}

void kdTree::build( std::set<Geometry*> obj, int depth )
{

  if( obj.size() < LEAF_SIZE ){
    objects = obj;
    return;
  }

  double xmin = bounds->min[0];
  double ymin = bounds->min[1];
  double zmin = bounds->min[2];
  double xmax = bounds->max[0];
  double ymax = bounds->max[1];
  double zmax = bounds->max[2];
  
  double xlength = xmax-xmin;
  double ylength = ymax-ymin;
  double zlength = zmax-zmin;

  BoundingBox *leftBBox = new BoundingBox(*bounds);
  BoundingBox *rightBBox = new BoundingBox(*bounds);

  // Cut the longest plane in half
  if( xlength >= ylength &&  xlength >= zlength ){
    //x is longest
    split = (xlength / 2) + xmin;
    axis = 0;
  } else if( ylength >= zlength ) {
    //y is longest
    split = (ylength / 2) + ymin;
    axis = 1;
  } else {
    //z is longest
    split = (zlength / 2) + zmin;
    axis = 2;
  }
  origSplit = split;

  typedef set<Geometry*>::const_iterator iter;

  leftBBox->max[axis] = split;
  rightBBox->min[axis] = split;

  std::set<Geometry*> leftObjects;
  std::set<Geometry*> rightObjects;

  double shift = 1.33;  // Percentage allowable past initial split
  for( iter j = obj.begin(); j != obj.end(); j++ ){
    const BoundingBox &bbox = (*j)->getBoundingBox();
    if( bbox.max[axis] <= split ){
      leftObjects.insert( *j );
    } else if( bbox.min[axis] > split ){
      rightObjects.insert( *j );
    } else {
    	  leftObjects.insert( *j );
    	  rightObjects.insert( *j );
    }
  }

  // If the recursion has reached the maximum allowable iterations
  if (depth <= 0) {
    objects = obj;
    return;
  }

  if(leftObjects.size()) {
    leftChild = new kdTree(leftBBox, leftObjects, depth-1);
  }
  if(rightObjects.size()) {
    rightChild = new kdTree(rightBBox, rightObjects, depth-1);
  }

}

kdTree::kdTree( BoundingBox *mBounds, std::set<Geometry*> obj, int depth ) :
  bounds(mBounds)
{
  leftChild = NULL;
  rightChild = NULL;
  build(obj, depth);
}
