#include <cmath>

#include "light.h"



using namespace std;

double DirectionalLight::distanceAttenuation( const Vec3d& P ) const
{
	// distance to light is infinite, so f(di) goes to 0.  Return 1.
	return 1.0;
}


Vec3d DirectionalLight::shadowAttenuation( const Vec3d& P ) const
{
    // YOUR CODE HERE:
    // You should implement shadow-handling code here.

	isect ii;

	Vec3d vecP;
	vecP[0]=P[0];
	vecP[1]=P[1];
	vecP[2]=P[2];

	Vec3d vecIntensity;
	vecIntensity[0]=1;
	vecIntensity[1]=1;
	vecIntensity[2]=1;

	while (1)
	{
		ray rr( vecP, -orientation, ray::SHADOW );
		if (scene->intersect(rr,ii))
		{
			if (ii.getMaterial().kt(ii)[0]==0) {
				return Vec3d(0,0,0);
			}
			else
			{
				vecP[0]=rr.at(ii.t+RAY_EPSILON)[0];//+2*RAY_EPSILON ;
				vecP[1]=rr.at(ii.t+RAY_EPSILON)[1];//+2*RAY_EPSILON ;
				vecP[2]=rr.at(ii.t+RAY_EPSILON)[2];//+2*RAY_EPSILON ;
				vecIntensity[0]*=ii.getMaterial().kt(ii)[0];
				vecIntensity[1]*=ii.getMaterial().kt(ii)[1];
				vecIntensity[2]*=ii.getMaterial().kt(ii)[2];

				if ((ii.N*rr.getDirection())<0)
				{

					// find the second intersection without searching
					ray rrr(vecP, -orientation, ray::SHADOW);
					isect iii;

					if (ii.obj->intersect(rrr,iii))
					{
						vecP[0]=rrr.at(iii.t+RAY_EPSILON)[0];//+2*RAY_EPSILON ;
						vecP[1]=rrr.at(iii.t+RAY_EPSILON)[1];//+2*RAY_EPSILON ;
						vecP[2]=rrr.at(iii.t+RAY_EPSILON)[2];//+2*RAY_EPSILON ;
					}					
				}
			}
		}
		else
			return vecIntensity;
	}
}

Vec3d DirectionalLight::getColor( const Vec3d& P ) const
{
	// Color doesn't depend on P 
	return color;
}

Vec3d DirectionalLight::getDirection( const Vec3d& P ) const
{
	return -orientation;
}

double PointLight::distanceAttenuation( const Vec3d& P ) const
{
	Vec3d vd = P - position;
	double d = sqrt(vd * vd);

	return min(1.0, 1/(constantTerm + linearTerm * d + quadraticTerm * d * d));

}

Vec3d PointLight::getColor( const Vec3d& P ) const
{
	// Color doesn't depend on P 
	return color;
}

Vec3d PointLight::getDirection( const Vec3d& P ) const
{
	Vec3d ret = position - P;
	ret.normalize();
	return ret;
}


Vec3d PointLight::shadowAttenuation(const Vec3d& P) const
{
    // YOUR CODE HERE:
    // You should implement shadow-handling code here.

	// direction from point light to intersection
	Vec3d vecDirLight;
	isect ii;


	Vec3d vecP;
	vecP[0]=P[0];
	vecP[1]=P[1];
	vecP[2]=P[2];

	Vec3d vecIntensity;
	vecIntensity[0]=1;
	vecIntensity[1]=1;
	vecIntensity[2]=1;

	while (1)
	{
		vecDirLight=getDirection(vecP);
		ray rr( vecP, vecDirLight, ray::SHADOW );
		if (scene->intersect(rr,ii))
		{
			Vec3d vecInter;
			// in case the intersection is behind the light
			vecInter=rr.at(ii.t);
			vecInter=getDirection(vecInter);
			if (vecInter*vecDirLight>0)
			{
				if (ii.getMaterial().kt(ii)[0]==0) {
					return Vec3d(0,0,0);
				}
				else
				{
					vecP[0]=rr.at(ii.t+2*RAY_EPSILON)[0];
					vecP[1]=rr.at(ii.t+2*RAY_EPSILON)[1];
					vecP[2]=rr.at(ii.t+2*RAY_EPSILON)[2];
					vecIntensity[0]*=ii.getMaterial().kt(ii)[0];
					vecIntensity[1]*=ii.getMaterial().kt(ii)[1];
					vecIntensity[2]*=ii.getMaterial().kt(ii)[2];

					if ((ii.N*rr.getDirection())<0)
					{
						// find the second intersection without searching
						vecDirLight=getDirection(vecP);
						ray rrr(vecP, vecDirLight, ray::SHADOW);
						isect iii;
						if (ii.obj->intersect(rrr,iii))
						{
							vecP[0]=rrr.at(iii.t+2*RAY_EPSILON)[0];//+2*RAY_EPSILON ;
							vecP[1]=rrr.at(iii.t+2*RAY_EPSILON)[1];//+2*RAY_EPSILON ;
							vecP[2]=rrr.at(iii.t+2*RAY_EPSILON)[2];//+2*RAY_EPSILON ;
						}
					}
				}
			}
			else
				return vecIntensity;
		}
		else
			return vecIntensity;
	}
}


double SpotLight::distanceAttenuation( const Vec3d& P ) const
{
	Vec3d vd1 = P - position;
	double angle; 
	
	vd1.normalize();
	
	angle = acos(vd1*direction)*180/3.14;

	if (angle>cutoff_angle) {
		return 0;
	}
	else {
		Vec3d vd = P - position;
		double d = sqrt(vd * vd);

		return min(1.0, 1/(constantTerm + linearTerm * d + quadraticTerm * d * d));
	}
}


Vec3d SpotLight::shadowAttenuation( const Vec3d& P ) const
{
 // YOUR CODE HERE:
    // You should implement shadow-handling code here.

	// direction from point light to intersection
	Vec3d vecDirLight;
	isect ii;


	Vec3d vecP;
	vecP[0]=P[0];
	vecP[1]=P[1];
	vecP[2]=P[2];

	Vec3d vecIntensity;
	vecIntensity[0]=1;
	vecIntensity[1]=1;
	vecIntensity[2]=1;

	while (1)
	{
		vecDirLight=getDirection(vecP);
		ray rr( vecP, vecDirLight, ray::SHADOW );

		double angle;
		angle = (direction*vecDirLight)*180/3.14;

		//printf("angle = %f\n", angle);
		if (angle>cutoff_angle) {
			return Vec3d(0,0,0);
		}
			
	
		if (scene->intersect(rr,ii))
		{
			Vec3d vecInter;
			// in case the intersection is behind the light
			vecInter=rr.at(ii.t);
			vecInter=getDirection(vecInter);
			if ((vecInter*vecDirLight>0))
			{
				if (ii.getMaterial().kt(ii)[0]==0) {
					return Vec3d(0,0,0);
				}
				else
				{
					vecP[0]=rr.at(ii.t+2*RAY_EPSILON)[0];
					vecP[1]=rr.at(ii.t+2*RAY_EPSILON)[1];
					vecP[2]=rr.at(ii.t+2*RAY_EPSILON)[2];
					vecIntensity[0]*=ii.getMaterial().kt(ii)[0];
					vecIntensity[1]*=ii.getMaterial().kt(ii)[1];
					vecIntensity[2]*=ii.getMaterial().kt(ii)[2];
				}
			}
			else
				return vecIntensity;
		}
		else
			return vecIntensity;
	}
}

Vec3d SpotLight::getColor( const Vec3d& P ) const
{
	// Color doesn't depend on P 
	return color;
}

Vec3d SpotLight::getDirection( const Vec3d& P ) const
{
	Vec3d ret = position - P;
	ret.normalize();

	return ret;
}

