//
// kdTree.h
//
// The class for creating kd-trees.
//

#pragma warning (disable: 4786)


#ifndef __KDTREE_H__
#define __KDTREE_H__

#include <set>
#include <algorithm>
#include <map>
#include <string>
#include <memory>

#include "scene.h"
#include "ray.h"
#include "../vecmath/vec.h"
#include "../vecmath/mat.h"

const int LEAF_SIZE = 15;

class BoundingBox;
class Geometry;

class kdTree
{
public:

  double split, origSplit;
  int axis;
  kdTree* leftChild;
  kdTree* rightChild;

public:
	kdTree( BoundingBox *mBounds, std::set<Geometry*> obj, int depth );

        void build(std::set<Geometry*> obj, int depth);
	
	bool intersect( const ray& r, isect& i, int& c );

	bool isLeaf() const { return leftChild==NULL && rightChild==NULL; }

private:
	std::set<Geometry*> objects;	

public:
	BoundingBox *bounds;
};

#endif // __KDTREE_H__
