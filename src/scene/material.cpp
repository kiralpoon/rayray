#include "ray.h"
#include "material.h"
#include "light.h"

#include "../fileio/bitmap.h"

using namespace std;
extern bool debugMode;

// Apply the phong model to this point on the surface of the object, returning
// the color of that point.
Vec3d Material::shade( Scene *scene, const ray& r, const isect& i ) const
{
	// YOUR CODE HERE

	// For now, this method just returns the diffuse color of the object.
	// This gives a single matte color for every distinct surface in the
	// scene, and that's it.  Simple, but enough to get you started.
	// (It's also inconsistent with the phong model...)

	// Your mission is to fill in this method with the rest of the phong
	// shading model, including the contributions of all the light sources.
    // You will need to call both distanceAttenuation() and shadowAttenuation()
    // somewhere in your code in order to compute shadows and light falloff.
	if( debugMode )
		std::cout << "Debugging Phong code..." << std::endl;

	// When you're iterating through the lights,
	// you'll want to use code that looks something
	// like this:
	//
	// for ( vector<Light*>::const_iterator litr = scene->beginLights(); 
	// 		litr != scene->endLights(); 
	// 		++litr )
	// {
	// 		Light* pLight = *litr;
	// 		.
	// 		.
	// 		.
	// }
	//return kd(i);

	
	// color of shade lights
	Vec3d vShade;
	// e and a
	vShade.n[0]=ke(i)[0]+ka(i)[0]*scene->ambient()[0];
	vShade.n[1]=ke(i)[1]+ka(i)[1]*scene->ambient()[1];
	vShade.n[2]=ke(i)[2]+ka(i)[2]*scene->ambient()[2];

	for ( vector<Light*>::const_iterator litr = scene->beginLights(); litr != scene->endLights(); ++litr )
	{
		Light* pLight = *litr;
		// attenuation
		Vec3d vAtten;
		vAtten=pLight->shadowAttenuation(r.at(i.t));
		vAtten*=pLight->distanceAttenuation(r.at(i.t));
		// direction
		Vec3d vDirLight;
		vDirLight=pLight->getDirection(r.at(i.t));
		double dNL=i.N*vDirLight;
		if (dNL<0)
			dNL=0;
		Vec3d vecR;
		vecR=2*(i.N*vDirLight)*i.N-vDirLight;
		vecR.normalize();
		double dRV=-1*(vecR*r.getDirection());
		if (dRV<0)
			dRV=0;
		dRV=pow(dRV,shininess(i));

		vShade[0]+=kd(i)[0]*pLight->getColor(r.at(i.t))[0]*dNL*vAtten[0];
		vShade[1]+=kd(i)[1]*pLight->getColor(r.at(i.t))[1]*dNL*vAtten[1];
		vShade[2]+=kd(i)[2]*pLight->getColor(r.at(i.t))[2]*dNL*vAtten[2];

		vShade[0]+=ks(i)[0]*pLight->getColor(r.at(i.t))[0]*dRV*vAtten[0];
		vShade[1]+=ks(i)[1]*pLight->getColor(r.at(i.t))[1]*dRV*vAtten[1];
		vShade[2]+=ks(i)[2]*pLight->getColor(r.at(i.t))[2]*dRV*vAtten[2];
	}
	

	return vShade;

}


TextureMap::TextureMap( string filename )
{
    data = readBMP( filename.c_str(), width, height );
    if( 0 == data )
    {
        width = 0;
        height = 0;
        string error( "Unable to load texture map '" );
        error.append( filename );
        error.append( "'." );
        throw TextureMapException( error );
    }
}

Vec3d TextureMap::getMappedValue( const Vec2d& coord ) const
{
	// YOUR CODE HERE

    // In order to add texture mapping support to the 
    // raytracer, you need to implement this function.
    // What this function should do is convert from
    // parametric space which is the unit square
    // [0, 1] x [0, 1] in 2-space to bitmap coordinates,
    // and use these to perform bilinear interpolation
    // of the values.



	if(data == NULL){
		return Vec3d(1.0, 1.0, 1.0);
	}
	else{
		double px = coord[0] * width;
		double py = coord[1] * height;
		int lx = (int)px;
		int gx = lx + 1;
		int ly = (int)py;
		int gy = ly + 1;
		Vec3d tmp1 = getPixelAt(lx, ly)*(gx - px) + getPixelAt(gx,ly)*(px - lx);
		Vec3d tmp2 = getPixelAt(lx, gy)*(gx - px) + getPixelAt(gx,gy)*(px - lx);
		return tmp1*(gy - py) + tmp2*(py - ly);
	}

}


Vec3d TextureMap::getPixelAt( int x, int y ) const
{
    // This keeps it from crashing if it can't load
    // the texture, but the person tries to render anyway.
    if (0 == data)
      return Vec3d(1.0, 1.0, 1.0);

    if( x >= width )
       x = width - 1;
    if( y >= height )
       y = height - 1;

    // Find the position in the big data array...
    int pos = (y * width + x) * 3;
    return Vec3d( double(data[pos]) / 255.0, 
       double(data[pos+1]) / 255.0,
       double(data[pos+2]) / 255.0 );
}

Vec3d MaterialParameter::value( const isect& is ) const
{
    if( 0 != _textureMap )
        return _textureMap->getMappedValue( is.uvCoordinates );
    else
        return _value;
}

double MaterialParameter::intensityValue( const isect& is ) const
{
    if( 0 != _textureMap )
    {
        Vec3d value( _textureMap->getMappedValue( is.uvCoordinates ) );
        return (0.299 * value[0]) + (0.587 * value[1]) + (0.114 * value[2]);
    }
    else
        return (0.299 * _value[0]) + (0.587 * _value[1]) + (0.114 * _value[2]);
}

