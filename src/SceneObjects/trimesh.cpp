#include <cmath>
#include <float.h>
#include "trimesh.h"

using namespace std;

Trimesh::~Trimesh()
{
	for( Materials::iterator i = materials.begin(); i != materials.end(); ++i )
		delete *i;
}

// must add vertices, normals, and materials IN ORDER
void Trimesh::addVertex( const Vec3d &v )
{
    vertices.push_back( v );
}

void Trimesh::addMaterial( Material *m )
{
    materials.push_back( m );
}

void Trimesh::addNormal( const Vec3d &n )
{
    Vec3d norm_n = n;
    norm_n.normalize();
    normals.push_back( norm_n );
}

// Returns false if the vertices a,b,c don't all exist
bool Trimesh::addFace( int a, int b, int c )
{
    int vcnt = vertices.size();

    if( a >= vcnt || b >= vcnt || c >= vcnt )
        return false;

    TrimeshFace *newFace = new TrimeshFace( scene, new Material(*this->material), this, a, b, c );
	Vec3d n = ((vertices[b] - vertices[a]) ^ (vertices[c] - vertices[a]));
	n.normalize();
	newFace->setN(n);
    newFace->setTransform(this->transform);
    faces.push_back( newFace );
    scene->add(newFace);
    return true;
}

char *
Trimesh::doubleCheck()
// Check to make sure that if we have per-vertex materials or normals
// they are the right number.
{
    if( !materials.empty() && materials.size() != vertices.size() )
        return (char *)"Bad Trimesh: Wrong number of materials.";
    if( !normals.empty() && normals.size() != vertices.size() )
        return (char *)"Bad Trimesh: Wrong number of normals.";

    return 0;
}

// Intersect ray r with the triangle abc.  If it hits returns true,
// and put the parameter in t and the barycentric coordinates of the
// intersection in bary.
// Uses the algorithm and notation from _Graphic Gems 5_, p. 232.
//
// Calculates and returns the normal of the triangle too.
bool TrimeshFace::intersectLocal( const ray& r, isect& i ) const
{
    const Vec3d& a = parent->vertices[ids[0]];
    const Vec3d& b = parent->vertices[ids[1]];
    const Vec3d& c = parent->vertices[ids[2]];

	Vec3d p = r.getPosition();
	Vec3d d = r.getDirection();
	Vec3d N;
	double tmp= d*normal;
	if(tmp == 0){
		return false;
	}
	else if(tmp > 0){
		N = - normal;
	}
	else{ 
		N = normal;
	}

	double t = (N * a - N * p) / (N * d);
	
	if(t < RAY_EPSILON)
		return false;

	Vec3d m = r.at(t);
	int k = -1;
	double maxcomp = -1;
	for(int idx = 0; idx < 3; idx++){
		if(abs(N[idx]) > maxcomp){
			maxcomp = abs(N[idx]);
			k = idx;
		}
	}

	double divisor = ((b - a) ^ (c - a))[k];
	if(divisor == 0.0){
		return false;
	}
	double u = ((m - a) ^ (c - a))[k] / divisor;
	double v = ((b - a) ^ (m - a))[k] / divisor;
	double w = 1 - u - v;
	if( u >= 0 && u <= 1 
		&& v >= 0 && v <= 1 
		&& w >= 0 && w <= 1){
			i.setT(t);
			i.setObject(this);
			if(parent->normals.empty()){
				i.setN(normal);
			}
			else{
				const Vec3d& na = parent->normals[ids[0]];
				const Vec3d& nb = parent->normals[ids[1]];
				const Vec3d& nc = parent->normals[ids[2]];
				
				i.setN(na*w + nb*u + nc*v);
			}

			if(!parent->materials.empty()){
				const Vec3d& da = parent->materials[ids[0]]->kd(i);
				const Vec3d& db = parent->materials[ids[1]]->kd(i);
				const Vec3d& dc = parent->materials[ids[2]]->kd(i);

				const Vec3d& sa = parent->materials[ids[0]]->ks(i);
				const Vec3d& sb = parent->materials[ids[1]]->ks(i);
				const Vec3d& sc = parent->materials[ids[2]]->ks(i);

				Material mat = *(parent->material);
				mat.setDiffuse(da*w + db*u + dc*v);
				mat.setSpecular(sa*w + sb*u + sc*v);
				i.setMaterial(mat);
			}
			return true;
	}

    return false;
}

void
Trimesh::generateNormals()
// Once you've loaded all the verts and faces, we can generate per
// vertex normals by averaging the normals of the neighboring faces.
{
     normals.clear();

     int cnt = vertices.size();
     normals.resize( cnt );	// initializes entries to the zero vector

     for( Faces::iterator fi = faces.begin(); fi != faces.end(); ++fi )
     {
         Vec3d a = vertices[(**fi)[0]];
         Vec3d b = vertices[(**fi)[1]];
         Vec3d c = vertices[(**fi)[2]];

         Vec3d faceNormal = ((b-a) ^ (c-a));
         faceNormal.normalize();

         for( int i = 0; i < 3; ++i )
         {
             normals[(**fi)[i]] += faceNormal;
         }
     }

     for( int i = 0; i < cnt; ++i )
     {
         normals[i].normalize();
     }
}

