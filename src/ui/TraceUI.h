//
// rayUI.h
//
// The header file for the UI part
//

#ifndef __rayUI_h__
#define __rayUI_h__

// who the hell cares if my identifiers are longer than 255 characters:
#pragma warning(disable : 4786)

#include <math.h>
#include "../vecmath/vec.h"
#include "../vecmath/mat.h"

#include <string>

using std::string;

class RayTracer;

class TraceUI {
public:
	TraceUI()
		: m_nDepth(0), m_nSize(150), m_nAASampleRate(1),
		m_usekdTree(1), m_useBackground(false),
		m_displayDebuggingInfo( false ),
		m_backWidth(0), m_backHeight(0), m_backGamma(1),
		raytracer( 0 )
	{ }

	virtual int		run() = 0;

	// Send an alert to the user in some manner
	virtual void		alert(const string& msg) = 0;

	// setters
	virtual void		setRayTracer( RayTracer* r )
		{ raytracer = r; }
	void setUseBackground( bool val)
		{ m_useBackground = val; }
	void setBackBuf(unsigned char* data)
	    { m_backBuf = data; }
	void setBackWidth(int width)
		{ m_backWidth = width; }
	void setBackHeight(int height)
		{ m_backHeight = height; }
	void setBackGamma(float gamma)
		{ m_backGamma = gamma; }

	// accessors:
	int		getSize() const { return m_nSize; }
	int		getDepth() const { return m_nDepth; }
	int		getAASampleRate() const { return m_nAASampleRate; }
	int		getUsekdTree() const { return m_usekdTree; }
	bool	getUseBackground() const { return m_useBackground; }
	unsigned char*	getBackBuf() const { return m_backBuf; }
	int		getBackWidth() const { return m_backWidth; }
	int		getBackHeight() const { return m_backHeight; }
	float	getBackGamma() const { return m_backGamma; }

protected:
	RayTracer*	raytracer;
	unsigned char* m_backBuf; 			// Buffer for the background image

	int			m_nSize;				// Size of the traced image
	int			m_nDepth;				// Max depth of recursion
	int			m_nAASampleRate;		// Anti-aliasing sample rate
	int			m_usekdTree;			// Use kdTree acceleration <0/1> = <false/true>

	// Determines whether or not to show debugging information
	// for individual rays.  Disabled by default for efficiency
	// reasons.
	bool		m_displayDebuggingInfo;

	bool		m_useBackground;		// Flagged true if there is a background image loaded
	int			m_backWidth;			// Width of the background image bitmap
	int			m_backHeight;			// Height of the background image bitmap
	float		m_backGamma;			// Gamma correction for the background

};

#endif
